package com.framework.learnHibernate.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "iot_group")
public class IotGroup implements Serializable {

    @Id @GeneratedValue
    @Column(name = "GroupId")
    private int groupId;

    @Column(name = "GroupName")
    private String groupName;

    @ManyToMany
    @JoinTable(
            name = "group_user",
            joinColumns = {@JoinColumn(name = "GroupId", referencedColumnName = "GroupId")},
            inverseJoinColumns = {@JoinColumn(name = "UserId", referencedColumnName = "UserId")}
    )
    private List<UserDetails> users = new ArrayList<>();

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<UserDetails> getUsers() {
        return users;
    }

    public void setUsers(List<UserDetails> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IotGroup iotGroup = (IotGroup) o;
        return groupId == iotGroup.groupId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId);
    }
}
