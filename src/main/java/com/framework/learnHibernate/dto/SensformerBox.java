package com.framework.learnHibernate.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "sensformerbox")
public class SensformerBox implements Serializable, Cloneable
{
    private static final long serialVersionUID = 1L;

    @Column(name = "SignalCount")
    private Integer signalCount;

    @EmbeddedId
    private SensformerBoxComposite sensformerBoxComposite = new SensformerBoxComposite();

    @OneToMany
    (mappedBy = "sensformerbox", cascade = CascadeType.ALL)
    private List<SensorLabel> labels = new ArrayList<>();

    public List<SensorLabel> getLabels() {
        return labels;
    }

    public void setLabels(List<SensorLabel> labels) {
        this.labels = labels;
    }

    public SensformerBoxComposite getSensformerBoxComposite() {
        return sensformerBoxComposite;
    }

    public void setSensformerBoxComposite(SensformerBoxComposite sensformerBoxComposite) {
        this.sensformerBoxComposite = sensformerBoxComposite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensformerBox that = (SensformerBox) o;
        return Objects.equals(sensformerBoxComposite, that.sensformerBoxComposite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sensformerBoxComposite);
    }
}
