package com.framework.learnHibernate.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SensformerBoxComposite implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "MacId")
    private String macId;

    @Column(name = "SensformerBoxName")
    private String sensformerBoxName;

    public SensformerBoxComposite(){}

    public SensformerBoxComposite(String macId, String boxName){
        this.macId = macId;
        this.sensformerBoxName = boxName;
    }

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    public String getSensformerBoxName() {
        return sensformerBoxName;
    }

    public void setSensformerBoxName(String sensformerBoxName) {
        this.sensformerBoxName = sensformerBoxName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensformerBoxComposite that = (SensformerBoxComposite) o;
        return Objects.equals(macId, that.macId) && Objects.equals(sensformerBoxName, that.sensformerBoxName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(macId, sensformerBoxName);
    }
}
