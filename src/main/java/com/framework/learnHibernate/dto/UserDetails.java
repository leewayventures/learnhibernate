package com.framework.learnHibernate.dto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "user_details")
public class UserDetails {
    @Id @GeneratedValue
    @Column(name = "UserId")
    private int userId;
    private String userName;

    @ManyToMany(mappedBy = "users")
    private List<IotGroup> iotGroups = new ArrayList<>();

    public UserDetails(){}

    public UserDetails(int userId, String userName){
        this.userId = userId;
        this.userName = userName;
    }
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<IotGroup> getIotGroups() {
        return iotGroups;
    }

    public void setIotGroups(List<IotGroup> iotGroups) {
        this.iotGroups = iotGroups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDetails that = (UserDetails) o;
        return userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }
}
