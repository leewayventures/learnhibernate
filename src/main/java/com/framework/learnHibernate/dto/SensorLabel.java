package com.framework.learnHibernate.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "sensorlabel")
public class SensorLabel implements Serializable
{
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private SensorLabelComposite compositeId;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "BoxMacId", referencedColumnName = "MacId"),
            @JoinColumn(name = "BoxName", referencedColumnName = "SensformerBoxName")
    })
    private SensformerBox sensformerbox;

    @Column(name = "SignalLabel")
    private String signalLabel;

    @Column(name = "SignalUnit")
    private String signalUnit;

    @Column(name = "is_deleted_from_ui")
    private Boolean isDeletedFromUI=false;

    public String getSignalLabel()
    {
        return signalLabel;
    }

    public void setSignalLabel(String signalLabel)
    {
        this.signalLabel = signalLabel;
    }

    public String getSignalUnit()
    {
        return signalUnit;
    }

    public void setSignalUnit(String signalUnit)
    {
        this.signalUnit = signalUnit;
    }
    public SensorLabelComposite getCompositeId()
    {
        return compositeId;
    }

    public void setCompositeId(SensorLabelComposite compositeId)
    {
        this.compositeId = compositeId;
    }

    public SensformerBox getSensformerbox()
    {
        return sensformerbox;
    }

    public void setSensformerbox(SensformerBox sensformerbox)
    {
        this.sensformerbox = sensformerbox;
    }

    public Boolean getDeletedFromUI() {
        return isDeletedFromUI;
    }

    public void setDeletedFromUI(Boolean deletedFromUI) {
        isDeletedFromUI = deletedFromUI;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorLabel that = (SensorLabel) o;
        return Objects.equals(compositeId, that.compositeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(compositeId);
    }

    @Override
    public String toString()
    {
        return this.getCompositeId().getMacId() + " : " + this.getCompositeId().getSignalId();
    }

}
