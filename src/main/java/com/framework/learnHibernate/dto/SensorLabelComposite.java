package com.framework.learnHibernate.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SensorLabelComposite implements Serializable
{

    private static final long serialVersionUID = 1L;

    @Column(name = "SignalId") // signalName coming from box is like Analog1,Analog2 etc
    private int signalId;

    @Column(name = "MacId")
    private String macId;

    /**
     * SensorLabelComposite Constructor
     */
    public SensorLabelComposite()
    {
        // SensorLabelComposite
    }

    public int getSignalId()
    {
        return signalId;
    }

    public void setSignalId(int signalId)
    {
        this.signalId = signalId;
    }

    public String getMacId()
    {
        return macId;
    }

    public void setMacId(String macId)
    {
        this.macId = macId;
    }

    @Override
    public String toString()
    {
        return this.getMacId() + " : " + this.getSignalId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorLabelComposite that = (SensorLabelComposite) o;
        return signalId == that.signalId && macId.equals(that.macId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(signalId, macId);
    }
}

