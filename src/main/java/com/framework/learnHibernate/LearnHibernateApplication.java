package com.framework.learnHibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnHibernateApplication.class, args);
	}

}
