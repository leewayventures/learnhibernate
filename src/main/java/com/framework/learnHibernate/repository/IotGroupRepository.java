package com.framework.learnHibernate.repository;

import com.framework.learnHibernate.dto.IotGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IotGroupRepository extends JpaRepository<IotGroup, Integer> {
}
