package com.framework.learnHibernate.repository;

import com.framework.learnHibernate.dto.SensformerBox;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SensformerBoxRepository extends JpaRepository<SensformerBox, String> {
}
