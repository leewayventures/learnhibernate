package com.framework.learnHibernate.repository;

import com.framework.learnHibernate.dto.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailsRepository extends JpaRepository<UserDetails, Integer> {
}
