package com.framework.learnHibernate.repository;

import com.framework.learnHibernate.dto.SensorLabel;
import com.framework.learnHibernate.dto.SensorLabelComposite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SensorLabelRepository extends JpaRepository<SensorLabel, SensorLabelComposite> {
}
