package com.framework.learnHibernate;

import com.framework.learnHibernate.dto.*;
import com.framework.learnHibernate.repository.IotGroupRepository;
import com.framework.learnHibernate.repository.SensformerBoxRepository;
import com.framework.learnHibernate.repository.SensorLabelRepository;
import com.framework.learnHibernate.repository.UserDetailsRepository;
import org.apache.catalina.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SensorLabelServiceUnitTest {
    //    Test concept of embedded key add sensor label
    @Autowired
    SensformerBoxRepository sensformerBoxRepository;

    @Autowired
    SensorLabelRepository sensorLabelRepository;

    @Autowired
    IotGroupRepository iotGroupRepository;

    @Autowired
    UserDetailsRepository userDetailsRepository;
    @Test
    public void whenSensorLabelAdded_thenCompositeKeyGetsCreated() {
        SensformerBox sensformerBox = new SensformerBox();
        String boxName = "BoxName";
        String macId1 = "MacId1";
        sensformerBox.setSensformerBoxComposite(new SensformerBoxComposite(macId1, boxName));

        SensorLabel sensorLabel1 = new SensorLabel();
        sensorLabel1.setSignalLabel("label1");
        sensorLabel1.setSignalUnit("pressure");
        sensorLabel1.setDeletedFromUI(true);

        SensorLabelComposite sensorLabelComposite1 = new SensorLabelComposite();
        sensorLabelComposite1.setMacId(macId1);
        sensorLabelComposite1.setSignalId(81);
        sensorLabel1.setCompositeId(sensorLabelComposite1);
        sensorLabel1.setSensformerbox(sensformerBox);

        SensorLabel sensorLabel2 = new SensorLabel();
        sensorLabel2.setSignalLabel("label2");
        sensorLabel2.setSignalUnit("pressure");
        sensorLabel2.setDeletedFromUI(false);

        SensorLabelComposite sensorLabelComposite2 = new SensorLabelComposite();
        sensorLabelComposite2.setMacId(macId1);
        sensorLabelComposite2.setSignalId(82);
        sensorLabel2.setCompositeId(sensorLabelComposite2);
        sensorLabel2.setSensformerbox(sensformerBox);


        List<SensorLabel> sensorLabelList = Arrays.asList(sensorLabel1, sensorLabel2);
        sensformerBox.getLabels().addAll(sensorLabelList);

        sensformerBoxRepository.save(sensformerBox);
        sensorLabelRepository.save(sensorLabel2);
        sensorLabelRepository.save(sensorLabel1);
    }

    @Test
    public void whenUserAdded_thenIotGroupMapped() {
        UserDetails user1 = new UserDetails();
        String userName1 = "user1";
        user1.setUserName(userName1);

        UserDetails user2 = new UserDetails();
        String userName2 = "user2";
        user2.setUserName(userName2);

        IotGroup group1 = new IotGroup();
        group1.setGroupName("group1");
        group1.getUsers().addAll(Arrays.asList(user1, user2));

        IotGroup group2 = new IotGroup();
        group2.setGroupName("group2");
        group2.getUsers().addAll(Arrays.asList(user1, user2));

        user1.getIotGroups().addAll(Arrays.asList(group1));
        user2.getIotGroups().addAll(Arrays.asList(group1));

        userDetailsRepository.save(user2);
        userDetailsRepository.save(user1);
        iotGroupRepository.save(group1);
        iotGroupRepository.save(group2);

    }
}